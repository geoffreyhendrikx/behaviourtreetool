﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Write your own properties here.
/// </summary>
[System.Serializable]
public class BlackBoard
{
    public Vector3 MoveToPosition;
    public GameObject MoveToObject;
}
