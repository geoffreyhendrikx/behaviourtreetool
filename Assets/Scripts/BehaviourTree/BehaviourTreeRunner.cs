﻿using System.Collections;
using UnityEngine;

public class BehaviourTreeRunner : MonoBehaviour
{
    public BehaviourTree tree;
    // Use this for initialization
    private void Start()
    {
        tree = tree.Clone();
        tree.Bind();
    }

    // Update is called once per frame
    void Update()
    {
        tree.Update();
    }
}
