using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatNode : DecoratorNode
{
    [Tooltip("Amount of seconds: looptime")]
    public float LoopTime;
    protected override void OnStart()
    {
        if (LoopTime == 0)
            LoopTime = Mathf.Infinity;
    }

    protected override void OnStop()
    {
    }

    protected override State OnUpdate()
    {
        if (LoopTime != Mathf.Infinity)
            LoopTime -= Time.deltaTime;
        if (LoopTime > 0)
        {
            child.Update();
            return State.Running;
        }
        else
            return State.Success;
    }
}
